var express = require('express');
const session = require('express-session');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var ejs = require('ejs');
const handlebars = require('express-handlebars');
var bodyParser = require('body-parser');
const cors = require('cors')
const path = require('path');
const fileUpload = require('express-fileupload');
const router = require('./controllers/SiteController');
const routerLogin = require('./routers/Login.router');
const routerDanhba = require('./routers/Danhba.router');
const Database = require('./config/data');



app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }));


// enable file upload
app.use(fileUpload({
    createParentPath: true,
    limits: { fileSize: 1000 * 1024 * 1024 }
}))


app.use(routerLogin);

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
})

http.listen(process.env.PORT || 3002, function () {
    console.log('Service running port 3002');
});