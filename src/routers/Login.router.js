const express = require('express')
const router = express.Router()
const LoginController = require('../controllers/Login.controller')
const DanhbaController = require('../controllers/Danhba.controller')
const chatController = require('../controllers/chat.controller')
const dangKiController = require('../controllers/SiteController')


// API login
router.get('/', LoginController.Page_Login)
router.post('/api/login', LoginController.post_login)

//API dang ki

router.post('/api/otp', dangKiController.guiOTP)
router.post('/api/kiemtraotp', dangKiController.kiemTraOTP)
router.post('/api/addNewUser', dangKiController.addNewUser)

//API danh bạ
router.get('/danhba', DanhbaController.Page_Danhba)
router.post('/api/danhba', DanhbaController.getAlldanhba)
router.post('/api/ketban', DanhbaController.ketban)
router.post('/api/xacyeucau', DanhbaController.xacyeucau)
router.post('/api/xoaban', DanhbaController.xoaban)
router.post('/api/themsdt', DanhbaController.themsdt)
router.post('/api/suasdt', DanhbaController.suasdt)
router.post('/api/xoasdt', DanhbaController.xoasdt)

//API chat 
router.get('/chat', chatController.chat)






module.exports = router