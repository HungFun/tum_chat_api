// Login
module.exports.LOGIN_SUCCESS = 'Đăng nhập thành công'
module.exports.LOGIN_FALSE = 'Sai mật khẩu hoặc tài khoản không tồn tại'
module.exports.LOGIN_ADMIN_FALSE = 'Bạn không phải ADMIN'


//Danh bạ
module.exports.FIND_USER_FALSE = 'Người dùng không tồn tại'
module.exports.FIND_USER_SUCCESS = 'Người dùng tồn tại'

//xóa bạn
module.exports.DELETE_FRIEND_FALSE= 'Xóa bạn bè thất bại'
module.exports.DELETE_FRIEND_SUCCESS= 'Xóa bạn bè thành công'

//xác nhận yêu cầu
module.exports.FIND_REQUEST_FRIEND_FALSE = "Tìm yêu cầu kết bạn thất bại"
module.exports.VERIFY_REQUEST_FRIEND_FALSE= 'xác nhận yêu cầu thất bại'
module.exports.VERIFY_REQUEST_FRIEND_SUCCESS= 'xác nhận yêu cầu thành công'
module.exports.NOT_VERIFY_FALSE ="Không đồng ý kết bạn thất bại"
module.exports.NOT_VERIFY_SUCCESS ="Không đồng ý kết bạn thành công"

//Kết bạn
module.exports.SEND_REQUEST_FRIEND_FALSE = 'Gửi yều cầu kết bạn thất bại'
module.exports.SEND_REQUEST_FRIEND_SUCCESS = 'Gửi yều cầu kết bạn thành công'
    // Đang kí tài khoàn
module.exports.SEND_REQUEST_OTP = 'Sai mã OTP'


//thêm danh bạ
module.exports.ADD_DIRECTORY_FALSE= 'Thêm danh bạ thất bại'
module.exports.ADD_DIRECTORY_SUCCESS= 'Thêm danh bạ thành công'
//xóa danh bạ
module.exports.DELETE_DIRECTORY_FALSE= 'Xóa danh bạ thất bại'
module.exports.DELETE_DIRECTORY_SUCCESS= 'Xóa danh bạ thành công'
//sửa danh bạ
module.exports.UPDATE_DIRECTORY_FALSE= 'Cập nhật danh bạ thất bại'
module.exports.UPDATE_DIRECTORY_SUCCESS= 'Cập nhật danh bạ thành công'



// module.exports.ERROR_FROM_MONGO = 'Error from db'
// module.exports.INVALID_VALUE = 'Invalid value'
// module.exports.FIND_SUCCESS = 'Find success'
// module.exports.ROOM_NOT_FOUND = 'Room not found'
// module.exports.DELETE_SUCCESS = 'Delete Success'
// module.exports.EXIT_SUCCESS = 'Exit Success'
// module.exports.UPDATE_SUCCESS = 'Update Success'
// module.exports.ADD_SUCCESS = 'Add Success'