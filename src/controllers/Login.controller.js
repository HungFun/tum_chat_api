var express = require('express');
const session = require('express-session');
var router = express.Router();
var user = require('../model/User');
var friend = require('../model/Friend');
var directory = require('../model/Directory');
const CONSTANT = require('../constants/room.constant')
const Response = require('../utils/response')
const routerLogin = require('../routers/Login.router');



require('dotenv').config()

/**
 * Trang Login
 *  */

const Page_Login = function(req, res) {
    res.render('Dangnhap.ejs');
};
/**
 * Thuc Hien Login
 *  */
const post_login = async(req, res) => {
    console.log(req.body.pass);
    user.findOne({ $or: [{ email: req.body.email }, { SDT: req.body.email }] }, function(err, myUser) {
        console.log(myUser);
        if (err) {
            res.send(new Response(true, CONSTANT.LOGIN_FALSE, null))
        }
        if (myUser != null) {
            if (myUser.email == "admin@gmail.com" || myUser.SDT == "0000") {
                res.send(new Response(true, CONSTANT.LOGIN_ADMIN_FALSE, null))
            } else if (myUser.matkhau == req.body.pass) {
                res.send(new Response(false, CONSTANT.LOGIN_SUCCESS, myUser))
            } else {
                res.send(new Response(true, CONSTANT.LOGIN_FALSE, null))
            }
        } else {
            res.send(new Response(true, CONSTANT.LOGIN_FALSE, null))
        }
    })
};


// const Page_dangki = function(req, res) {
//     res.render('DangkiTaikhoan.ejs')
// }

module.exports = {
    Page_Login,
    post_login
    // Page_dangki
};