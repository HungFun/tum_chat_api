var express = require('express');
const session = require('express-session');
var router = express.Router();
var user = require('./../model/User');
var friend = require('./../model/Friend');
var directory = require('./../model/Directory');
const Response = require('../utils/response')
const CONSTANT = require('../constants/room.constant')

require('dotenv').config()
/**
 * Trang danh ba
 *  */
const Page_Danhba = function (req, res) {
  
    res.render('danhba.ejs');
};
/**
 * Lấy dữ liệu
 *  */
const getAlldanhba = function (req, res) {
    var nguoidung =req.body.SDT;
    friend.find({ sdtNguoiGui: nguoidung }).then(listfriends => {
        friend.find({ sdtNguoiNhan: nguoidung }).then(listrequest => {
                user.find({}).then(getAlluser => {
                    directory.find({ mysdt: nguoidung }).then(directories => {
                        var useradd = { listfriends, listrequest, directories, getAlluser }
                        res.send({ user: useradd})
                    })
            })
        })
    })
};

/**
 * Gui Yeu Cau kết bạn
 *  */
const ketban = function (req, res) {
    var myuser = req.body.user;
    user.findOne({ SDT: req.body.sdtuser }).then(usersend => {
        var newfiend = new friend({
            sdtNguoiGui: myuser.SDT,
            tenNguoiGui: myuser.ho + myuser.ten,
            avatarNguoiGui: myuser.avatar,
            tenNguoiNhan: usersend.ho + " " + usersend.ten,
            sdtNguoiNhan: usersend.SDT,
            avatarNguoiNhan: usersend.avatar,
            emailNguoiNhan: usersend.email,
            ngaysinhNguoiNhan: usersend.ngaysinh,
            gioitinhNguoiNhan: usersend.gioitinh,
            trangthaixacnhan: 'false',
            ngayxacnhan: '',
        });
        newfiend.save(function(err,sendfriend){
            if(err){
                res.send(new Response(false, CONSTANT.SEND_REQUEST_FRIEND_FALSE, null))
            }
            res.send(new Response(true, CONSTANT.SEND_REQUEST_FRIEND_SUCCESS, null))
        });
    });
};
/**
 * Xóa bạn trong danh bạ bạn bè
 *  */
const xoaban = function (req, res) {
    var nguoidung =req.body.SDT;
    friend.findOneAndDelete({ $and: [{ sdtNguoiGui: req.body.sdtuser }, { sdtNguoiNhan: nguoidung }] },
        function(err1,x){
            if(err1){
                console.log("Xóa bạn bên người kia" + x);
                res.send(new Response(false, CONSTANT.DELETE_FRIEND_FALSE, null))
            }
    });
    friend.findOneAndDelete({ $and: [{ sdtNguoiGui: nguoidung }, { sdtNguoiNhan: req.body.sdtuser }] },
        function(err2,y){
            if(err2){
                console.log("Xóa bạn bên mình" + y);
                res.send(new Response(false, CONSTANT.DELETE_FRIEND_FALSE, null))
            }
    });
    res.send(new Response(true, CONSTANT.DELETE_FRIEND_SUCCESS, null))
};

/**
 *xác Nhận yêu cầu kết bạn
 *  */
const xacyeucau = function (req, res) {
    var myuser = req.body.user;
    var ts = Date.now();
    let date_ob = new Date(ts);
    let date = date_ob.getDate();
    let month = date_ob.getMonth() + 1;
    let year = date_ob.getFullYear();
    var time = year + "-" + month + "-" + date;
    if (req.body.xacnhan == "yes") {
        friend.findOneAndUpdate({ $and: [{ sdtNguoiGui: req.body.sdtuser }, { sdtNguoiNhan: myuser.SDT }] }, 
            { $set: { trangthaixacnhan: 'true', ngayxacnhan: time + "" } },function(errYes,requestfriend){
                if(errYes){
                    res.send(new Response(false, CONSTANT.FIND_REQUEST_FRIEND_FALSE, null))
                }
                else{
                    var newfiend = new friend({
                        sdtNguoiGui: requestfriend.sdtNguoiNhan,
                        tenNguoiGui: requestfriend.tenNguoiNhan,
                        avatarNguoiGui: requestfriend.avatarNguoiNhan,
                        tenNguoiNhan: requestfriend.tenNguoiGui,
                        sdtNguoiNhan: requestfriend.sdtNguoiGui,
                        avatarNguoiNhan: requestfriend.avatarNguoiGui,
                        emailNguoiNhan: myuser.email,
                        ngaysinhNguoiNhan: myuser.ngaysinh,
                        gioitinhNguoiNhan: myuser.gioitinh,
                        trangthaixacnhan: 'true',
                        ngayxacnhan: time + "",
                    });
                    newfiend.save(function(errnew,myfriend){
                        if (errnew) {
                            res.send(new Response(false, CONSTANT.VERIFY_REQUEST_FRIEND_FALSE, null))
                        }
                        else{
                            res.send(new Response(true, CONSTANT.VERIFY_REQUEST_FRIEND_SUCCESS, null))
                        }
                    });
                }
            });
    } else if (req.body.xacnhan == "no") {
        friend.findOneAndDelete({ $and: [{ sdtNguoiGui: req.body.sdtuser }, 
            { sdtNguoiNhan: myuser.SDT }] },function(errNo,removeFriend){
                console.log(removeFriend);
                if (errNo) {
                    res.send(new Response(false, CONSTANT.NOT_VERIFY_FALSE, null))
                }
                else{
                    res.send(new Response(true, CONSTANT.NOT_VERIFY_SUCCESS, null))
                }
            });
    } 
};


/**
 * Thêm số điện thoại vào danh bạ
 *  */
const themsdt = function (req, res) {
    var newdirectory = new directory({
        mysdt: req.body.SDT,
        ten: req.body.newName,
        sdt: req.body.newNumber,
    });
    newdirectory.save(function(err,directoryadd){ 
        if (err){ 
            res.send(new Response(true, CONSTANT.ADD_DIRECTORY_FALSE, null));
        } 
        else{
            directory.find({mysdt:req.body.SDT}).then(directories=>{
                res.send(new Response(false, CONSTANT.ADD_DIRECTORY_SUCCESS,directories));
            }); 
        } 
    }) 
};
/**
 * Sua so dien thoai
 *  */
const suasdt = function (req, res) {
    directory.findByIdAndUpdate({_id:req.body.idSuaThongTin}, { $set: { ten: req.body.newName, sdt: req.body.newNumber } },function (err, directoryupdate){
            if (err){ 
                res.send(new Response(true, CONSTANT.UPDATE_DIRECTORY_FALSE, null));
            } 
            else{
                directory.find({mysdt:req.body.SDT}).then(directories=>{
                    res.send(new Response(false, CONSTANT.UPDATE_DIRECTORY_SUCCESS,directories));
                }); 
            } 
        });
};
/**
 * Xoa so dien thoai
 *  */
const xoasdt = function (req, res) {
    directory.findByIdAndDelete(req.body.idXoaThongTin,function (err, directorydelete){
        if (err){ 
            res.send(new Response(true, CONSTANT.DELETE_DIRECTORY_FALSE, null));
        } 
        else{
            directory.find({mysdt:req.body.SDT}).then(directories=>{
                res.send(new Response(false, CONSTANT.DELETE_DIRECTORY_SUCCESS,directories));
            }); 
        } 
    });
};
module.exports = {
    getAlldanhba,
    Page_Danhba,
    ketban,
    xacyeucau,
    xoaban,
    themsdt,
    suasdt,
    xoasdt
};